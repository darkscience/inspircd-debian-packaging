#!/usr/bin/env bash
set -e
set -u

ver=3.7.0

if $(grep -qs docker /proc/1/cgroup); then
  DOCKER=1
fi

test ! -z ${DOCKER+x} && \
  apt-get update && \
  apt-get install sudo -y && \
  cd /build;

test -f /usr/bin/gcc || \
  sudo apt-get -y install build-essential devscripts debhelper
  # Assuming assumptions
  sudo apt-get -y install debhelper perl libgnutls28-dev pkg-config \
               libldap2-dev libpcre3-dev default-libmysqlclient-dev libpq-dev \
               libsqlite3-dev zlib1g-dev libgeoip-dev \
               libtre-dev dh-systemd dh-apparmor wget

test -f inspircd_${ver}.orig.tar.gz || \
  wget https://github.com/inspircd/inspircd/archive/v${ver}.tar.gz -O inspircd_${ver}.orig.tar.gz

(
  mkdir insp-build
  cp -r debian insp-build/
  cd insp-build
  tar xf ../inspircd_${ver}.orig.tar.gz --strip-components=1
  debuild -us -uc
  test ! -z ${DOCKER+x} && \
	 cp /build/*.deb /out/

)
