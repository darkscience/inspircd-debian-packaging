docker run \
  -it \
  --name insp-build \
  -v "$(pwd)"/debian:/build/debian \
  -v "$(pwd)"/out:/out \
  -v "$(pwd)"/build.insp.sh:/build/build.insp.sh:ro,rslave \
  -w /build/ \
  debian:latest \
  bash /build/build.insp.sh

docker rm insp-build
